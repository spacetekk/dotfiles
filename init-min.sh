#!/bin/sh

DOTFILES=$HOME/.config/dotfiles
ln -sf $DOTFILES/.inputrc $HOME/.inputrc
ln -sf $DOTFILES/.vimrc.min $HOME/.vimrc
ln -sf $DOTFILES/.zshenv $HOME/.zshenv
ln -sf $DOTFILES/.zshrc $HOME/.zshrc

# clone vim bundle
if [[ -d $HOME/.vim ]]; then
    mv -f  $HOME/.vim  $HOME/.vim.dotfiles-old
fi

mkdir --parents $HOME/.vim/bundle
git clone https://github.com/gmarik/vundle.git $HOME/.vim/bundle/vundle
