" deprecated {{{
" set nocompatible
" filetype off

" set rtp+=~/.vim/bundle/vundle/
" call vundle#rc()

" Plugin 'gmarik/vundle'
" Plugin 'nelstrom/vim-qargs'
" Plugin 'scrooloose/syntastic'
" Plugin 'othree/html5.vim'
" Plugin 'kchmck/vim-coffee-script'
" Plugin 'digitaltoad/vim-jade'
" Plugin 'dag/genshi.vim'
" Plugin 'powerline/powerline', {'rtp': 'powerline/bindings/vim'}
" Plugin 'KabbAmine/zeavim.vim'
" Plugin 'Glench/Vim-Jinja2-Syntax'
" Plugin 'wavded/vim-stylus'

" Plugin 'spacetekk/myvim'
" Plugin 'spacetekk/pgsql.vim'
" }}}

syntax on
filetype on
filetype plugin on
filetype plugin indent on

" Disable:
" --------
set nocompatible   " nocompatible mode
set novisualbell   " no error bell
set nobackup       " no backup files
set noswapfile     " no swap files
set mousehide      " no mouse
set noinfercase    " 'as is' matches in completion

" Indent:
" -------
set tabstop=4
set shiftwidth=4
set shiftround
set smarttab
set expandtab
set softtabstop=4
set autoindent
set smartindent

" Search:
" -------
set incsearch
set ignorecase
set smartcase

set wrap
set linebreak
set cursorline
set autowrite
set autowriteall
set number
set relativenumber
set wildmenu
set t_Co=256
set bs=2
set termencoding=utf-8
set fileencodings=utf8,cp1251
set encoding=utf8
set laststatus=2
set wildmode=full
set diffopt=vertical
set listchars=tab:▸\ ,eol:¬
set updatetime=100

" set complete=.,w,b,u,t,k,i
set complete=.,w,b,u,t,k
set completeopt=menu,preview

" History And Undo:
" -----------------
set history=9999
set undolevels=128
set undodir=/tmp/undodir/
set undofile
set undolevels=9999
set undoreload=99999
set viminfo='9999,f1,n/tmp/.viminfo

set foldmethod=marker
set shortmess+=I
set isfname-== " remove '=' from valid file name chars, for <c-f> on var=/tmp/fnam...

let mapleader=" "

runtime macros/matchit.vim

" Netrw:
" ------
let g:netrw_liststyle = 2
" let g:netrw_liststyle = 3

" Mappings:
" ---------
" view all with: help index

" Completion:
" -----------
inoremap <c-f> <c-x><c-f>
inoremap <c-o> <c-x><c-o>
inoremap <c-l> <c-x><c-l>

" Buffers:
" --------
nnoremap <silent> <leader>h :bprevious<cr>
nnoremap <silent> <leader>l :bnext<cr>

" Tabs:
" nnoremap <silent> <c-t> :tabnew<cr>

" Write Delete Buffer:
" --------------------
nnoremap <leader><cr> :write!<cr>
nnoremap <bs> :bunload!<cr>
nnoremap <bs><bs> :bdelete!<cr>
nnoremap <leader><bs> :bufdo bd!<cr>

" Cmd:
" ----
cmap <bs> <nop>
cnoremap <c-p> <up>
cnoremap <c-n> <down>

" Exit Vim:
" ---------
nnoremap Q :qall!<cr>
nnoremap <f10> :!!<cr>

" Local:
" ------
try
    source ~/.vimrc.local
catch
endtry
